﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SchedulerExample;

namespace SchedulerExample.TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            IJobService myJobService1 = JobManager.Scheduler()
                .ToRunOnceIn(5)
                .Seconds()
                .AtStartTime()
                .BuildJobService<MyJob>();

            IJobService myJobService2 = JobManager.Scheduler()
                .ToRunOnceIn(1)
                .Seconds()
                .AtTheEndOfDay()
                .BuildJobService<MyJob1>();

            var cancellationTokenSource = new CancellationTokenSource();
            var token = cancellationTokenSource.Token;

            myJobService1.Start(token);
            myJobService2.Start(token);

            Thread.Sleep(10000);
            myJobService1.Stop();
            myJobService1.Dispose();
            Thread.Sleep(15000);

            cancellationTokenSource.Cancel();

            myJobService1.StartAsync(token);
            Thread.Sleep(5000);

            cancellationTokenSource.Cancel();
            cancellationTokenSource.Dispose();

            Console.ReadLine();
        }
    }

    public class MyJob : IJob
    {
        public void Execute()
        {
            Console.WriteLine("1st job" + DateTime.Now.ToString("yyyy MM dd HH:mm:ss"));
        }
    }
    public class MyJob1 : IJob
    {
        public void Execute()
        {
            Console.WriteLine("2nd job" + DateTime.Now.ToString("yyyy MM dd HH:mm:ss"));
        }
    }
}
