﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchedulerExample.Builders
{
    class JobIntervalDateBuilder : IJobIntervalDateBuilder
    {
        private readonly float interval;
        public JobIntervalDateBuilder() => interval = 0;
        public JobIntervalDateBuilder(float intervalValue) => interval = intervalValue;

        public IJobDueTimeBuilder Seconds()
        {
            return new JobDueTimeBuilder(interval, IntervalTypes.Second);
        }

        public IJobDueTimeBuilder Minutes()
        {
            return new JobDueTimeBuilder(interval, IntervalTypes.Minute);
        }

        public IJobDueTimeBuilder Hours()
        {
            return new JobDueTimeBuilder(interval, IntervalTypes.Hour);
        }

        public IJobDueTimeBuilder Days()
        {
            return new JobDueTimeBuilder(interval, IntervalTypes.Day);
        }
    }
}
