﻿namespace SchedulerExample.Builders
{
    public interface IJobStartBuilder
    {
        IJobIntervalDateBuilder ToRunOnceIn(float interval);
    }
}