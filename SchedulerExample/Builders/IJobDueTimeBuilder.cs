﻿using System;

namespace SchedulerExample.Builders
{
    public interface IJobDueTimeBuilder
    {
        IJobServiceBuilder At(DateTime dateTime);
        IJobServiceBuilder AtStartTime();
        IJobServiceBuilder AtTheEndOfDay();
        IJobServiceBuilder AtTomorrowStartOfDay();
    }
}