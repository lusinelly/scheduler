﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchedulerExample.Builders
{
    class JobServiceBuilder : IJobServiceBuilder
    {
        private readonly IntervalTypes intervalType;
        private readonly float interval;
        private readonly TimeSpan startTime;

        public JobServiceBuilder()
        {
            intervalType = IntervalTypes.Second;
            interval = 0;
            startTime = TimeSpan.Zero;
        }

        public JobServiceBuilder(TimeSpan startTimeValue, float intervalValue, IntervalTypes intervalTypeValue) => (startTime, interval, intervalType) = (startTimeValue, intervalValue, intervalTypeValue);
      
        IJobService IJobServiceBuilder.BuildJobService<TJob>()
        {
            var js = new JobService<TJob>
            {
                Interval = interval,
                IntervalType = intervalType,
                StartTime = startTime,
            };

            return js;
        }

        IJobService IJobServiceBuilder.BuildJobService<TJob>(Func<TJob> createJob)
        {
            var js = new JobService<TJob>(createJob)
            {
                Interval = interval,
                IntervalType = intervalType,
                StartTime = startTime,
            };

            return js;
        }
    }
}
