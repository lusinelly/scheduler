﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchedulerExample.Builders
{
    class JobDueTimeBuilder : IJobDueTimeBuilder
    {
        private readonly IntervalTypes intervalType;
        private readonly float interval;

        public JobDueTimeBuilder()
        {
            intervalType = IntervalTypes.Second;
            interval = 0;
        }
        public JobDueTimeBuilder(float intervalValue, IntervalTypes intervalTypeValue) => (interval, intervalType) = (intervalValue, intervalTypeValue);

        public IJobServiceBuilder At(DateTime dateTime)
        {
            DateTime now = DateTime.Now;

            if (dateTime < now)
                throw new InvalidOperationException("Given date is earlier than now");

            TimeSpan startTime = dateTime - now;
            return new JobServiceBuilder(startTime, interval, intervalType);
        }

        public IJobServiceBuilder AtStartTime()
        {
            return new JobServiceBuilder(TimeSpan.Zero, interval, intervalType);
        }

        public IJobServiceBuilder AtTheEndOfDay()
        {
           DateTime now = DateTime.Now;
           TimeSpan startTime = new DateTime(now.Year, now.Month, now.Day, 23, 59, 59) - DateTime.Now;
           return new JobServiceBuilder(TimeSpan.Zero, interval, intervalType);
        }

        public IJobServiceBuilder AtTomorrowStartOfDay()
        {
            DateTime tomorrow = DateTime.Now.AddDays(1);
            TimeSpan startTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 0, 0, 0, 0) - DateTime.Now;

            return new JobServiceBuilder(startTime, interval, intervalType);
        }
    }

}
