﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SchedulerExample
{
    public class JobService<TJob> : IJobService 
        where TJob : IJob, new()
    {
        public IntervalTypes IntervalType { get; set; }
        public float Interval  { get; set; }
        public TimeSpan StartTime  { get; set; }
        public TJob Job { get; set; }
        public Timer JobTimer { get; set; }

        public JobService() => (IntervalType, Interval, StartTime, Job) = (IntervalTypes.Second, 0, TimeSpan.Zero, new TJob());
        public JobService(Func<TJob> createJob) => Job = createJob.Invoke();
        
        public void Start(CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            TimeSpan period;

            if (cancellationToken.IsCancellationRequested)
            {
                Console.WriteLine("Cancelling at task.");
                cancellationToken.ThrowIfCancellationRequested();
            }

            switch (IntervalType)
            { case IntervalTypes.Second:
                    period = TimeSpan.FromSeconds(Interval);
                    break;
                case IntervalTypes.Minute:
                    period = TimeSpan.FromMinutes(Interval);
                    break;
                case IntervalTypes.Hour:
                    period = TimeSpan.FromHours(Interval);
                    break;
                case IntervalTypes.Day:
                    period = TimeSpan.FromDays(Interval);
                    break;
                default:
                    break;
            }

            JobTimer = new Timer(x =>
            {
                Job.Execute();

            }, null, StartTime, period);
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            Task task = Task.Factory.StartNew((y) => {

                cancellationToken.ThrowIfCancellationRequested();
                TimeSpan period;

                if (cancellationToken.IsCancellationRequested)
                {
                    Console.WriteLine("Cancelling at task.");
                    cancellationToken.ThrowIfCancellationRequested();
                }

                switch (IntervalType)
                {
                    case IntervalTypes.Second:
                        period = TimeSpan.FromSeconds(Interval);
                        break;
                    case IntervalTypes.Minute:
                        period = TimeSpan.FromMinutes(Interval);
                        break;
                    case IntervalTypes.Hour:
                        period = TimeSpan.FromHours(Interval);
                        break;
                    case IntervalTypes.Day:
                        period = TimeSpan.FromDays(Interval);
                        break;
                    default:
                        break;
                }

                JobTimer = new Timer(x =>
                {
                    Job.Execute();

                }, null, StartTime, period);
            }, cancellationToken, cancellationToken);

            return task;
        }

        public void Stop()
        {
            JobTimer.Change(Timeout.Infinite, Timeout.Infinite);
        }

        public Task StopAsync()
        {
            return Task.Factory.StartNew(() => { JobTimer.Change(Timeout.Infinite, Timeout.Infinite); });
        }

        public Task<bool> ChangeAsync(int dueTime, int period)
        {
            return Task<bool>.Factory.StartNew(() => { return JobTimer.Change(dueTime, period); });
        }

        public void Dispose()
        {
            JobTimer.Dispose();
        }
    }

    public enum IntervalTypes
    {
        Second = 0,
        Minute,
        Hour,
        Day
    }
}