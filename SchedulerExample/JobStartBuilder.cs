﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchedulerExample.Builders
{
    class JobStartBuilder : IJobStartBuilder
    {
        public IJobIntervalDateBuilder ToRunOnceIn(float interval)
        {
            return new JobIntervalDateBuilder(interval);
        }
    }
}
